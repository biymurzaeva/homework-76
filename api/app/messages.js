const express = require('express');
const fileDB = require('../fileDB');
const router = express.Router();

router.get('/', (req, res) => {
	const date = req.query.datetime;

	const messages = fileDB.getItems();
	const timeMsg = fileDB.getNewItems(date);

	if (date) {
		if (isNaN(new Date(date))) {
			return res.status(400).send({error: "Invalid date"});
		} else {
			res.send(timeMsg || []);
		}
	}
	res.send(messages);
});

router.post('/', (req, res) => {
	if (!req.body.author || !req.body.message) {
		return res.status(400).send({error: "Author and message must be present in the request"});
	}
	const newMessage = fileDB.addItem({
		author: req.body.author,
		message: req.body.message,
	});

	res.send(newMessage);
});

module.exports = router;