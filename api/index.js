const express = require('express');
const messages = require('./app/messages');
const cors = require('cors');
const fileDB = require('./fileDB');

const app = express();
app.use(express.json());
app.use(cors());

const port = 8000;
app.use('/messages', messages);

fileDB.init();
app.listen(port, () => {
	console.log(`Server started on ${port} port!`);
});