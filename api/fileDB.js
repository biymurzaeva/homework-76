const fs = require('fs');
const {nanoid} = require('nanoid');
const filename = './db.json';
let data = [];

module.exports = {
	init() {
		try {
			const fileContents = fs.readFileSync(filename);
			data = JSON.parse(fileContents);
		} catch (e) {
			data = [];
		}
	},
	getItems() {
		return data.slice(-30);
	},
	getNewItems(time) {
		return data.filter(d => {
			if (d.datetime > time) {
				return true;
			}
		});
	},
	addItem(item) {
		item.id = nanoid();
		item.datetime = (new Date()).toISOString();
		data.push(item);
		this.save();
		return item;
	},
	save() {
		fs.writeFileSync(filename, JSON.stringify(data));
	}
};