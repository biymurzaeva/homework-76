import {Route, Switch} from "react-router-dom";
import Messages from "./containers/Messages/Messages";
import AddProduct from "./containers/AddProduct/AddProduct";

const App = () => (
  <>
    <Switch>
      <Route path='/' exact component={Messages}/>
      <Route path='/messages/add' component={AddProduct}/>
      <Route path='/messages' component={Messages}/>
    </Switch>
  </>
);

export default App;
