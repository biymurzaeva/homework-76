import axios from "axios";

export const FETCH_MESSAGES_REQUEST = 'FETCH_MESSAGES_REQUEST';
export const FETCH_MESSAGES_SUCCESS = 'FETCH_MESSAGES_SUCCESS';
export const FETCH_MESSAGES_FAILURE = 'FETCH_MESSAGES_FAILURE';

export const FETCH_TIME_MESSAGES_REQUEST = 'FETCH_TIME_MESSAGES_REQUEST';
export const FETCH_TIME_MESSAGES_SUCCESS = 'FETCH_TIME_MESSAGES_SUCCESS';
export const FETCH_TIME_MESSAGES_FAILURE = 'FETCH_TIME_MESSAGES_FAILURE';

export const ADD_NEW_MESSAGE_REQUEST = 'ADD_NEW_MESSAGE_REQUEST';
export const ADD_NEW_MESSAGE_SUCCESS = 'ADD_NEW_MESSAGE_SUCCESS';
export const ADD_NEW_MESSAGE_FAILURE = 'ADD_NEW_MESSAGE_FAILURE';

export const fetchMessagesRequest = () => ({type: FETCH_MESSAGES_REQUEST});
export const fetchMessagesSuccess = messages => ({type: FETCH_MESSAGES_SUCCESS, payload: messages});
export const fetchMessagesFailure = () => ({type: FETCH_MESSAGES_FAILURE});

export const fetchTimeMessagesRequest = () => ({type: FETCH_TIME_MESSAGES_REQUEST});
export const fetchTimeMessagesSuccess = timeMessages => ({type: FETCH_TIME_MESSAGES_SUCCESS, payload: timeMessages});
export const fetchTimeMessagesFailure = () => ({type: FETCH_TIME_MESSAGES_FAILURE});

export const addNewMessageRequest = () => ({type: ADD_NEW_MESSAGE_REQUEST});
export const addNewMessageSuccess = () => ({type: ADD_NEW_MESSAGE_SUCCESS});
export const addNewMessageFailure = () => ({type: ADD_NEW_MESSAGE_FAILURE});

export const fetchMessages = () => {
	return async dispatch => {
		try {
			dispatch(fetchMessagesRequest());
			const response = await axios.get('http://127.0.0.1:8000/messages');
			dispatch(fetchMessagesSuccess(response.data));
		} catch (e) {
			dispatch(fetchMessagesFailure());
			throw e;
		}
	};
};

export const fetchTimeMessages = datetime => {
	return async dispatch => {
		try {
			dispatch(fetchTimeMessagesRequest());
			const response = await axios.get(`http://127.0.0.1:8000/messages?datetime=${datetime}`);
			dispatch(fetchTimeMessagesSuccess(response.data));
		} catch (e) {
			dispatch(fetchTimeMessagesFailure());
			throw e;
		}
	};
};


export const addNewMessage = messageData => {
	return async dispatch => {
		try {
			dispatch(addNewMessageRequest());
			await axios.post(`http://127.0.0.1:8000/messages`, messageData);
			dispatch(addNewMessageSuccess());
		} catch (e) {
			dispatch(addNewMessageFailure());
			throw e;
		}
	};
};

