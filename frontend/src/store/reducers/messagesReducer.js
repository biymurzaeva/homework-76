import {
	FETCH_MESSAGES_FAILURE,
	FETCH_MESSAGES_REQUEST,
	FETCH_MESSAGES_SUCCESS,
	FETCH_TIME_MESSAGES_FAILURE,
	FETCH_TIME_MESSAGES_REQUEST,
	FETCH_TIME_MESSAGES_SUCCESS
} from "../actions/messagesAction";

const initialState = {
	fetchLoading: false,
	timeLoading: false,
	messages: [],
	timeMessages: null
};

const messagesReducer = (state = initialState, action) => {
	switch (action.type) {
		case FETCH_MESSAGES_REQUEST:
			return {...state, fetchLoading: true};
		case FETCH_MESSAGES_SUCCESS:
			return {...state, fetchLoading: false, messages: action.payload};
		case FETCH_MESSAGES_FAILURE:
			return {...state, fetchLoading: false}
		case FETCH_TIME_MESSAGES_REQUEST:
			return {...state, timeLoading: true};
		case FETCH_TIME_MESSAGES_SUCCESS:
			return {...state, timeLoading: false, timeMessages: action.payload};
		case FETCH_TIME_MESSAGES_FAILURE:
			return {...state, timeLoading: false}
		default:
			return state;
	}
};

export default messagesReducer;