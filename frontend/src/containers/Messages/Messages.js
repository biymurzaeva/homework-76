import React, {useEffect, useState} from 'react';
import {Link} from "react-router-dom";
import {Button, Grid, Typography} from "@material-ui/core";
import Message from "../../components/Message/Message";
import {useDispatch, useSelector} from "react-redux";
import {fetchMessages, fetchTimeMessages} from "../../store/actions/messagesAction";

const Messages = ({location}) => {
	const dispatch = useDispatch();
	const fullMessages = useSelector(state => state.messages.messages);
	const timeMessages = useSelector(state => state.messages.timeMessages);

	const parseDatetime = () => {
		const params = new URLSearchParams(location.search);
		return Object.fromEntries(params).datetime;
	};

	const [datetime] = useState(parseDatetime());

	useEffect(() => {
		dispatch(fetchMessages());
		if (datetime) {
			dispatch(fetchTimeMessages(datetime));
		}
	}, [dispatch, datetime]);

	let messages = fullMessages;

	if (timeMessages) {
		messages = timeMessages;
	}

	return messages && (
		<Grid container direction="column" spacing={2}>
			<Grid item container justifyContent="space-between" alignItems="center">
				<Grid item>
					<Typography variant="h4">Messages</Typography>
				</Grid>
				<Grid>
					<Button color="primary" component={Link} to="messages/add">Add new message</Button>
				</Grid>
			</Grid>
			<Grid item container direction="row" spacing={1}>
				{messages.map(product => (
					<Message
						key={product.id}
						id={product.id}
						author={product.author}
						message={product.message}
					/>
				))}
			</Grid>
		</Grid>
	);
};

export default Messages;