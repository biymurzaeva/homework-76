import React from 'react';
import MessageForm from "../../components/MessageForm/MessageForm";
import {Typography} from "@material-ui/core";
import {useDispatch} from "react-redux";
import {addNewMessage} from "../../store/actions/messagesAction";

const AddProduct = ({history}) => {
	const dispatch = useDispatch();

	const onSubmit = async messageData => {
		await dispatch(addNewMessage(messageData));
		history.replace('/');
	};

	return (
		<>
			<Typography variant="h4">New product</Typography>
			<MessageForm
				onSubmit={onSubmit}
			/>
		</>
	);
};

export default AddProduct;