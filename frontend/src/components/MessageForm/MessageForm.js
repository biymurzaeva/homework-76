import {Button, Grid, makeStyles, TextareaAutosize, TextField} from "@material-ui/core";
import {useState} from "react";

const useStyles = makeStyles(theme => ({
	root: {
		marginTop: theme.spacing(2)
	},
	textarea: {
		padding: theme.spacing(2)
	},
}));

const MessageForm = ({onSubmit}) => {
	const classes = useStyles();

	const [state, setState] = useState({
		author: "",
		message: "",
	});

	const submitFormHandler = e => {
		e.preventDefault();
		onSubmit({...state});
	};

	const inputChangeHandler = e => {
		const name = e.target.name;
		const value = e.target.value;
		setState(prevState => {
			return {...prevState, [name]: value};
		});
	};

	return (
		<Grid
			container
			direction="column"
			spacing={2}
			component="form"
			className={classes.root}
			autoComplete="off"
			onSubmit={submitFormHandler}
		>
			<Grid item xs>
				<TextField
					fullWidth
					variant="outlined"
					name="author"
					label="Author"
					value={state.author}
					onChange={inputChangeHandler}
				/>
			</Grid>
			<Grid item xs>
				<TextareaAutosize
					minRows={8}
					placeholder="Messages"
					name="message"
					value={state.message}
					onChange={inputChangeHandler}
					className={classes.textarea}
				/>
			</Grid>
			<Grid item xs>
				<Button type="submit" color="primary" variant="contained">Add</Button>
			</Grid>
		</Grid>
	);
};

export default MessageForm;