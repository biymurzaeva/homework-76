import React from 'react';
import {Card, CardContent, CardHeader, Grid, Typography} from "@material-ui/core";

const Message = ({author, message}) => {
	return (
		<Grid item xs={12} sm={12} md={6} lg={4}>
			<Card>
				<CardHeader title={author} variant="h3"/>
				<CardContent>
					<Typography variant="subtitle1">
						{message}
					</Typography>
				</CardContent>
			</Card>
		</Grid>
	);
};

export default Message;
